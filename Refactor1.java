/*
 * Clase para refactorizar
 * Codificacion UTF-8
 * Paquete: ra3.refactorizacion
 * Se deben comentar todas las refactorizaciones realizadas,
 * mediante comentarios de una linea o de bloque.
 */
package ra3; //Importo paquete

import java.util.Scanner; // Importo cada clase en una linea separada.
/**
 * 
 * @author fvaldeon
 * @since 31-01-2018
 */
public class Refactor1 { // Cambio nombre de la clase porque tiene que ser igual que el fichero.
	final static String cad = "Bienvenido al programa";
	public static void main(String[] args) {
		int numeroA = 7;
		// Refactorizacion de int b, escribiendola en una nueva linea.
		int numeroB = 16;
		//Inicializo la variable
		String dni;
		// Refactorizacion de String cadena2, escribiendola en una nueva linea.
		String nombre;
		
		Scanner lector = new Scanner(System.in); // Separar por secciones (zona de creacion de variables, zona Scanner.
		System.out.println(cad);
		System.out.println("Introduce tu dni");
		dni = lector.nextLine(); // Yo crearia aqui el String pero no hay ningun convenio.
		System.out.println("Introduce tu nombre");
		nombre = lector.nextLine();
		System.out.println("El usuario es " + nombre + " " + dni);
		
		int numeroC = 25;
		if(numeroA > numeroB || numeroC % 5 != 0 
				&& (numeroC * 3 - 1) > numeroB / numeroC){ 
			//Separacion para mejor lectura y posicionar el corchete bien y despues del && cambiar de linea.
			System.out.println("Se cumple la condición");
		}
		
		numeroC = numeroA + numeroB * numeroC + numeroB / numeroA; //Separo las operaciones.
		
		String array[] = {"Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"};
		// Inicializar el array completa y no posicion por posicion.
		
		recorrerArray(array);
		lector.close();//Cierro el scanner
	}
	static void recorrerArray(String vectorDeStrings[]){ // Cambio de nombre el metodo y el String recibido y poner el { en su posicion.
		for(int dia = 0; dia < 7; dia++){ // Separar operaciones y poner corchete bien.
			System.out.println("El dia de la semana en el que te encuentras [" + (dia + 1) + "-7] es el dia: " + vectorDeStrings[dia]);
			// Adecuar la operaciones.
		}
	}
	
}
