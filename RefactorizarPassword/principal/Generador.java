package principal;

import java.util.Scanner;

public class Generador {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Programa que genera passwords de la longitud indicada, y del rango de caracteres");
		mostrarMenu();
		int longitud = scanner.nextInt();
		System.out.println("Elige tipo de password: ");
		int opcion = scanner.nextInt();
		String password = "";
		
		for (int i = 0; i < longitud; i++) {
			switch (opcion) {
			case 1:
				password += letraAleatorio();
				break;
			case 2:
				password += numeroAleatorio();
				break;
			case 3:		
				password += generarLetrasCarEsp();
				break;	
			case 4:
				password += generarLetrasCarEspNumeros();
				break;
			}
		}
		System.out.println("La contraseņa es " + password);
		scanner.close();
	}
	
	public static void mostrarMenu (){
		System.out.println("1 - Caracteres desde A - Z");
		System.out.println("2 - Numeros del 0 al 9");
		System.out.println("3 - Letras y caracteres especiales");
		System.out.println("4 - Letras, numeros y caracteres especiales");
		System.out.println("Introduce la longitud de la cadena: ");
	}
	
	private static char letraAleatorio(){
		return (char) ((Math.random() * 26) + 65);
	}
	
	private static int numeroAleatorio(){
		return (int) (Math.random() * 10);
	}
	
	private static char caracterEspAleatorio(){
		return (char) ((Math.random() * 15) + 33);
	}
	
	private static char generarLetrasCarEsp(){
		char caracter;
		int n = (int) (Math.random() * 2);
		if (n == 1) {
			caracter = letraAleatorio();
		} else {
			caracter = caracterEspAleatorio();
		} 
		return caracter;
	}
	
	private static char generarLetrasCarEspNumeros(){
		char caracter;
		int n = (int) (Math.random() * 3);
		if (n == 1) {
			caracter = letraAleatorio();
		} else if (n == 2) {
			caracter = caracterEspAleatorio();
		} else {
			caracter = (char) numeroAleatorio();
		}
		return caracter;
	}
}
