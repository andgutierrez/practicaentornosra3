/*
 * Clase para refactorizar
 * Codificacion UTF-8
 * Paquete: ra3.refactorizacion
 * Se deben comentar todas las refactorizaciones realizadas,
 * mediante comentarios de una linea o de bloque.
 */
package ra3; //Importo paquete
import java.util.Scanner; // Importo cada clase en una linea separada.
/**
 * 
 * @author fvaldeon
 * @since 31-01-2018
 */
public class Refactor2 { // Cambio nombre de la clase porque tiene que ser igual que el fichero.

	static Scanner lector = new Scanner(System.in);//Creo, cambio el nombre e inicializo el scanner
	public static void main(String[] args){
		
		int n; //Cada variable en una linea
		int cantidadMaximaAlumnos = 10; //Renombro la variable y la inicializo
		
		int arrays[] = new int[cantidadMaximaAlumnos]; //Cambio la capacidad del array para que cambio automaticamente si cambia la cantidad maxima de alumnos
		for(n = 0; n < 10; n++){ //Separo y coloco bien el "{"
			System.out.println("Introduce nota media de alumno");
			arrays[n] = lector.nextInt();
		}	
		System.out.println("El resultado es: " + recorrerArray(arrays));
		
		lector.close();
	}
	static double recorrerArray(int vector[])	{ //Coloco bien el "{" y renombro el metodo
		double suma = 0; //Renombro
		for(int i = 0; i < 10; i++) { //Separo y coloco bien el "{" y cambio en nombre de la variable iterativa a "i"
			suma = suma + vector[i]; //Separo
		}
		return suma/10;
	}	
}
